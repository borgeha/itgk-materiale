# ITGK-materiale

## Mac og Linux

### Klone prosjektet ved bruk av Git
For å få tilgang til materialet som ligger her på Gitlab, må du først installere git på PC-en. 
Guide på hvordan du laster det ned på MacOS, Linux eller Windows finner du [her](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

For å klone prosjektet på din PC, må du åpne terminalen, naviger i en mappe du ønsker å ha prosjektet lagret i og skriv følgende kommando:
`git clone https://gitlab.stud.idi.ntnu.no/borgeha/itgk-materiale.git`
Terminalen ber deg nå om å oppgi brukernavn og passordet ditt som du bruker til å logge inn på Gitlab.
Nå skal prosjektet være lagret på pc-en din. 

For å få oppdaterte endringer, må du pulle prosjektet i terminalen. Husk å være i selve mappen der gitlab prosjektet ligger før du gjør det.
For å pulle skriver du følgende kommando:
`git pull  https://gitlab.stud.idi.ntnu.no/borgeha/itgk-materiale.git master`


### Installere extensions på MacOS og Linux 
For å få riktig format opp på våre Jupyter Notebooks, må dere ha lastet ned extensions lokalt på PC-en inntil videre. Dere må ha pip installert allerede på pc-en. Hvis du lurer på hvordan man laster ned 
pip, er det en guide i denne linken [her](https://www.shellhacks.com/python-install-pip-mac-ubuntu-centos/ )
Følg så følgende instrukser:
*  Åpne terminalen
*  Skriv følgende kommandolinjer 
`pip install jupyter_contrib_nbextensions` og etterfulgt av
`jupyter contrib nbextension install --user` på en ny linje.

Vi anbefaler at dere også har configurator lastet ned så dere enkelt kan velge hvilke extensions dere skal bruke direkte i notebooken. Dette lastes ned på følgende måte:
`pip install jupyter_nbextensions_configurator` For å aktivere configurator, skriver du en følgende i en ny kommandolinje: 
`jupyter nbextensions_configurator enable --user`
Kommentar: Ingen av oss har klart å få extensions til å fungere på Windows 10 enda


Trykk [her](https://gitlab.stud.idi.ntnu.no/borgeha/itgk-materiale/-/wikis/Skjermbilder) for å se skjermbilder til hvordan interface nå vil se ut med både nbextensions og configurator installert



